#!/usr/bin/python2.7
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright (c) 2008, 2015, Oracle and/or its affiliates. All rights reserved.
#

"""
Most of the generic unix methods of our superclass can be used on
Solaris. For the following methods, there is a Solaris-specific
implementation in the 'arch' extension module.
"""

import os
import subprocess
import tempfile

from os_unix import \
    get_group_by_name, get_user_by_name, get_name_by_gid, get_name_by_uid, \
    is_admin, get_userid, get_username, chown, rename, remove, link, \
    copyfile, split_path, get_root, assert_mode
from pkg.portable import ELF, EXEC, PD_LOCAL_PATH, UNFOUND, SMF_MANIFEST

import pkg.arch as arch
from pkg.sysattr import fgetattr, fsetattr
from pkg.sysattr import get_attr_dict as get_sysattr_dict

def get_isainfo():
        return arch.get_isainfo()

def get_release():
        return arch.get_release()

def get_platform():
        return arch.get_platform()

def get_file_type(actions):
        from pkg.flavor.smf_manifest import is_smf_manifest
        for a in actions:
                path = a.attrs['path']
                lpath = a.attrs[PD_LOCAL_PATH]
                try:
                    with open(lpath, 'r') as f:
                        if os.fstat(f.fileno()).st_size == 0:
                            yield "empty file"
                            continue
                        magic = f.read(4)
                except FileNotFoundError:
                        yield UNFOUND
                        continue
                if magic == '\x7fELF':
                        yield ELF
                elif magic[:2] == '#!':
                        yield EXEC
                elif ((path.startswith('var/svc/manifest/') or
                    path.startswith('lib/svc/manifest/')) and
                    is_smf_manifest(lpath)):
                        yield SMF_MANIFEST
                else:
                        yield "unknown"
